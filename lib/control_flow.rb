# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  uppercase_string = ""
  str.chars.each do |ch|
    if ch == ch.upcase
      uppercase_string << ch
    end
  end

  uppercase_string
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length/2
  if str.length.odd?
    str[mid]
  else
    str[mid - 1..mid]
  end
end

# Return the number of vowels in a string.

def num_vowels(str)
  vowels = "aeiou"
  count = 0
  str.chars.each do |ch|
    if vowels.include?(ch.downcase)
      count += 1
    end
  end

  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)

end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  i = 0
  join = ""

  while i < arr.length
    join = join + arr[i]

    if i != arr.length - 1
      join = join + separator
    end

    i += 1
  end

  join
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.each_index do |idx|
    if idx.odd?
      str[idx] = str[idx].upcase
    else
      str[idx] = str[idx].downcase
    end
  end

    str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split

  arr.map! do |word|
    if word.length >= 5
      word = word.reverse
    else
      word = word
    end
  end

  arr.join(" ")
end
# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz = []

  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      fizzbuzz << "fizzbuzz"
    elsif num % 3 == 0
      fizzbuzz << "fizz"
    elsif num % 5 == 0
      fizzbuzz << "buzz"
    else
      fizzbuzz << num
    end
  end

  fizzbuzz
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_arr = []

  arr.each do |el|
    reversed_arr.unshift(el)
  end

  reversed_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each do |div|
    if num % div == 0
      return false
    end
  end
  true

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |factor| factor if num % factor == 0 }

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |prime| prime if prime?(prime) }

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []

  arr.each do |num|
    if num.odd?
      odds << num
    else
      evens << num
    end
  end

  if evens.length > 1
    return odds[0]
  end

  evens[0]
end
